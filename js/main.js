/**
 * Created by karelcech on 31/03/2017.
 */

function hideOverlayOnMobile() {
    if (/Mobile|Android|webOS|iPhone|Nexus|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        $('#youtubeBackgroundOverlay').hide();
    }
}