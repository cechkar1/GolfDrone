/**
 * Created by karelcech on 30/03/2017.
 */


var player;

var fadeInTime = 5000;
var fadeOutTime = 500;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('youtubeBackground', {
        videoId: 'MoMFEME_FD4',
        playerVars: {
            'loop': 1,
            'controls': 0,
            'autoplay': 1,
            'showinfo': 0,
            'iv_load_policy': 3,
            'disablekb': 1,
            'rel': 0,
            'modestbranding': 0,
            'start': 0,
            'playlist': '0xdLE90DvHY'

        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange,
            'onPlaybackQualityChange': onPlaybackQualityChange,
        }
    });
}

function setupPlayer() {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    onYouTubeIframeAPIReady();
}

function onPlayerReady() {
    player.setPlaybackQuality('hd1080');
    player.mute();
}

function onPlayerStateChange(event) {
    var vidSaver = $('#videoSaver');
    if (event.data == YT.PlayerState.PLAYING && player.getPlaybackQuality().search('h') >= 0) {
        vidSaver.fadeOut(fadeOutTime);
        return;
    }
    vidSaver.fadeIn(fadeInTime);
}

function onPlaybackQualityChange() {
    var vidSaver = $('#videoSaver');
    if (player.getPlaybackQuality().search('h') >= 0 && player.getPlayerState == YT.PlayerState.PLAYING) {
        vidSaver.fadeOut(fadeOutTime);
        return;
    }
    vidSaver.fadeIn(fadeInTime);
    if(player.getPlaybackQuality().search('h') < 0)
        console.log('low quality!! ' + player.getPlaybackQuality());
}
function getBestVideoWidth() {
    if (screen.height > (screen.width * 9) / 16) {
        return ((screen.height * 16) / 9)
    }
    return screen.width;
}

function setBackgroundDimensions() {
    var width = getBestVideoWidth();
    $('#youtubeBackground,#youtubeBackgroundOverlay').css({
        width: width,
        height: (screen.width * 9) / 16
    })
}


$(window).on("orientationchange", function (event) {
    setBackgroundDimensions();
});