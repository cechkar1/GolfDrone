/**
 * Created by karelcech on 22/03/2017.
 */
function getRandomBackgroundFile(){
    var files =  ['src/videos/background.mp4','src/videos/backgroundv3.mp4'];
    return files[Math.floor(Math.random() * files.length)];
}

function setBGVideo(oldId) {
    var ids = {bgvid1: 'bgvid2', bgvid2: 'bgvid1'};
    var exists = false;
    for (var key in ids) {
        exists = false;
        if (ids[key] == oldId) {
            exists = true;
            break;
        }
    }
    if(!exists) oldId='bgvid2';
    var currentId = ids[oldId];
    var vidObj = $('#' + currentId);
    vidObj.fadeIn(1000);
    var vid = vidObj.find('source')[0];
    var oldSrc = vid.src;
    do{
        vid.src = getRandomBackgroundFile();
    } while(oldSrc===vid.src);

    vidObj[0].load();
    vidObj[0].pause();
    vidObj[0].currentTime = 0;
    if(vid.src == 'file:///Users/karelcech/GolfDrone/Web/src/videos/background.mp4') vidObj[0].currentTime = '185';
    vidObj[0].play();
    vidObj.one('pause', function () {
        vidObj.fadeOut(1000);
        setBGVideo(currentId);
    });
}
